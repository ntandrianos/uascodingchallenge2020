# Niko Andrianos
# 2020-09-14
# UAS Coding challenge - Line Intersection Challenge (B-LNE-0)

import math

 # quadratic equation built using the two equations:
 # y=mx+b line equation
 # (x-x0)^2 + (y-y0)^2 = r^2 circle equation
 # depending on the number of roots we can determine the num. of intersections
 # no roots = no intersections, 1 roots = 1 ints., 2 roots = 2 ints.
 # function then returns number of intersections

def get_num_line_intersection(radius, x_circle, y_circle, x1, x2, y1, y2):

    num_intersections = 0
    root1=0
    root2=0

    # calculate the coeff.(s) for y=mx+b

    m = (y2-y1)/(x2-x1)
    b = y1-m*x1

    # calculate the coeff.(s) for qudratic eqauation
    # coeff.(s) determined by solving the two equations by hand
    # equation: (1+m^2)x^2 + 2(m(b-y_circle)-x_circle)x + x_circle^2 + (b-y_circle)^2-r^2 = 0

    alpha = b-y_circle # small calc to make more readable
    a = 1+m**2
    b = 2*(m*alpha-x_circle)
    c = x_circle**2+alpha**2-radius**2

    discrim = b**2-4*a*c

    # Based on the value of the discriminant, we can deduce the number of solutions
    # Calculate roots (intersection points) for the user, not needed to determine the number of intersections

    if discrim<0:
        print("No roots")
        num_intersections=0

    elif discrim==0:
        num_intersections=1
        one_root[0] = -1*b/(2*a)
     
    else:
        num_intersections=2
       
        two_roots[0] = (-1*b-math.sqrt(discrim))/(2*a)
        two_roots[1] = (-1*b+math.sqrt(discrim))/(2*a)

    print("Number of intersections:", end = ' ')

    return (num_intersections)

# Return lists to store roots 
two_roots = [0,0]
one_root = [0]

print("Test case 1")
# No intersection test case
test_rad = 4
test_x0 = 10
test_y0 = -5

test_x1=0
test_x2=15

test_y1=-10
test_y2=15

print(get_num_line_intersection(test_rad,test_x0,test_y0,test_x1,test_x2,test_y1,test_y2))
print()

print("Test case 2")
# One intersection test case
test_rad = 10
test_x0 = 12
test_y0 = 0

test_x1=0
test_x2=30

test_y1=10
test_y2=10

print(get_num_line_intersection(test_rad,test_x0,test_y0,test_x1,test_x2,test_y1,test_y2))
print("Roots:", end = " ")
print(*one_root, sep = ", ")
print()

print("Test case 3")
# Two intersection test case
test_rad = 5
test_x0 = 9
test_y0 = 3

test_x1=0
test_x2=15

test_y1=-10
test_y2=15

print(get_num_line_intersection(test_rad,test_x0,test_y0,test_x1,test_x2,test_y1,test_y2))
print("Roots:", end = " ")
print(*two_roots, sep = ", ")
print()

# END